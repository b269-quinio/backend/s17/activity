/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printWelcomeMessage(){
	let fullName = prompt("What is your name? ");
	let realAge = prompt("How old are you? ");
	let fullAddress = prompt("What city do you live in? ");

	console.log("Hello, " + fullName);
	console.log("You are " + realAge + " years old.");
	console.log("You live in " + fullAddress + " City");
}

printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

alert ("Kindly input your top 5 favorite bands of all time, in descending order.")

function printBand1(){
	let Band1 = prompt("1st favorite band: ");
	console.log("1. " + Band1);
}
printBand1();

function printBand2(){
	let Band2 = prompt("2nd favorite band: ");
	console.log("2. " + Band2);
}
printBand2();

function printBand3(){
	let Band3 = prompt("3rd favorite band: ");
	console.log("3. " + Band3);
}
printBand3();

function printBand4(){
	let Band4 = prompt("4th favorite band: ");
	console.log("4. " + Band4);
}
printBand4();

function printBand5(){
	let Band5 = prompt("5th favorite band: ");
	console.log("5. " + Band5);
}
printBand5();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

alert ("Kindly input your top 5 favorite movies of all time, in descending order.")

function printMovie1(){
	let Movie1 = prompt("1st favorite Movie: ");
	let RatingMovie1 = "94%"
	console.log("1. " + Movie1);
	console.log("Rotten Tomatoes Rating: " + RatingMovie1)
}
printMovie1();

function printMovie2(){
	let Movie2 = prompt("2nd favorite Movie: ");
	let RatingMovie2 = "89%"
	console.log("2. " + Movie2);
	console.log("Rotten Tomatoes Rating: " + RatingMovie2)
}
printMovie2();

function printMovie3(){
	let Movie3 = prompt("3rd favorite Movie: ");
	let RatingMovie3 = "65%"
	console.log("3. " + Movie3);
	console.log("Rotten Tomatoes Rating: " + RatingMovie3)
}
printMovie3();

function printMovie4(){
	let Movie4 = prompt("4th favorite Movie: ");
	let RatingMovie4 = "76%"
	console.log("4. " + Movie4);
	console.log("Rotten Tomatoes Rating: " + RatingMovie4)
}
printMovie4();

function printMovie5(){
	let Movie5 = prompt("5th favorite Movie: ");
	let RatingMovie5 = "84%"
	console.log("5. " + Movie5);
	console.log("Rotten Tomatoes Rating: " + RatingMovie5)
}
printMovie5();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers(); ->dragged down and rename to printFriends();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}
printFriends();

// console.log(friend1);
// console.log(friend2);
